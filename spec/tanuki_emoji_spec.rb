# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe TanukiEmoji do
  let(:horse_emoji) do
    described_class::Character.new('horse',
      codepoints: "\u{1f434}",
      alpha_code: ':horse:',
      description: 'horse face',
      category: 'nature')
  end

  let(:mage_emoji) do
    described_class::Character.new('mage',
      codepoints: "\u{1f9d9}",
      alpha_code: ':mage:',
      description: 'mage',
      category: 'people')
  end

  let(:lighter_skin_mage) do
    described_class::Character.new('mage: light skin tone',
      codepoints: "\u{1f9d9}\u{1f3fb}",
      alpha_code: ':mage_tone1:',
      description: 'mage with lighter skin',
      category: 'people')
  end

  before do
    described_class.index.reset!(reload: false)
  end

  it 'has a version number' do
    expect(described_class::VERSION).not_to be_nil
  end

  describe '.add' do
    it 'initializes a new Character and adds to the index' do
      described_class.add(horse_emoji.name,
        codepoints: horse_emoji.codepoints,
        alpha_code: horse_emoji.alpha_code,
        description: horse_emoji.description,
        category: horse_emoji.category)

      expect(described_class.index.all).to include(horse_emoji)
    end
  end

  describe '.index' do
    it 'returns a list of known and indexed Emojis' do
      described_class.index.reset!

      expect(described_class.index.all.count).to be > 1000
    end
  end

  describe '.find_by_alpha_code' do
    before do
      described_class.index.add(horse_emoji)
      described_class.index.add(mage_emoji)
    end

    it 'returns an existing Emoji that matches provided :alpha_code:' do
      emoji = described_class.find_by_alpha_code(horse_emoji.alpha_code)

      expect(emoji).not_to be_nil
      expect(emoji).to eq(horse_emoji)
    end

    it 'returns an existing Emoji that matches provided alpha_code even when colon delimiter is missed' do
      emoji = described_class.find_by_alpha_code('horse')

      expect(emoji).not_to be_nil
      expect(emoji).to eq(horse_emoji)
    end

    it 'returns nil when provided :alpha_code: doesnt match existing emojis' do
      emoji = described_class.find_by_alpha_code('nonexistent_code')

      expect(emoji).to be_nil
    end
  end

  describe '.find_by_codepoints' do
    before do
      described_class.index.add(horse_emoji)
      described_class.index.add(mage_emoji)
      described_class.index.add(lighter_skin_mage)
    end

    it 'returns nil when provided codepoint doesnt match existing Emojis' do
      emoji = described_class.find_by_codepoints('😀') # "\u{1f600}"

      expect(emoji).to be_nil
    end

    context 'with emojis using single codepoint' do
      it 'returns an existing Emoji that matches provided codepoints' do
        emoji = described_class.find_by_codepoints('🐴') # "\u{1f434}"

        expect(emoji).not_to be_nil
        expect(emoji).to eq(horse_emoji)
      end
    end

    context 'with emojis with modifiers' do
      it 'returns a matching Emoji indexed with respective modifier' do
        emoji = described_class.find_by_codepoints(lighter_skin_mage.codepoints)

        expect(emoji).not_to be_nil
        expect(emoji).to eq(lighter_skin_mage)
      end
    end

    context 'with emojis with selectors' do
      it 'when text variant selector is present, returns nil even if a matching Emoji would otherwise exist' do
        emoji = described_class.find_by_codepoints("\u{1f434}\u{fe0e}")

        expect(emoji).to be_nil
      end
    end
  end
end
