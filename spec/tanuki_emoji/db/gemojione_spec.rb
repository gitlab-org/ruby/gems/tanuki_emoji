# frozen_string_literal: true

require 'rspec'

RSpec.describe TanukiEmoji::Db::Gemojione do
  # These codes from gemojione 3.3.0 are no longer found in the Unicode emojis,
  # nor are they in gemojione 4.1.0. So ignore them
  let(:emoji_exceptions) do
    {
      '🤼🏻' => 'wrestlers_tone1',
      '🤼🏼' => 'wrestlers_tone2',
      '🤼🏽' => 'wrestlers_tone3',
      '🤼🏾' => 'wrestlers_tone4',
      '🤼🏿' => 'wrestlers_tone5'
    }
  end

  let(:gemoji_db) do
    File.open(TanukiEmoji::Db::Gemojione::DATA_FILE, 'r:UTF-8') do |file|
      JSON.parse(file.read, symbolize_names: true)
    end
  end

  before do
    TanukiEmoji::Index.instance.reset!
  end

  it 'contains all codepoints', :aggregate_failures do
    gemoji_db.each_value do |emoji_data|
      next if emoji_exceptions.key?(emoji_data[:moji])

      emoji = TanukiEmoji.find_by_codepoints(emoji_data[:moji])

      expect(emoji.codepoints == emoji_data[:moji] || emoji.codepoints_alternates.include?(emoji_data[:moji])).to be_truthy
    end
  end

  it 'alpha_codes correspond to correct emojis', :aggregate_failures do
    gemoji_db.each_value do |emoji_data|
      next if emoji_exceptions.key?(emoji_data[:moji])

      emoji = TanukiEmoji.find_by_alpha_code(emoji_data[:shortname])

      expect(emoji.codepoints == emoji_data[:moji] || emoji.codepoints_alternates.include?(emoji_data[:moji])).to be_truthy
    end
  end

  it 'ensures a gemojione code is not used as an alias', :aggregate_failures do
    db_shortnames = gemoji_db.values.map { |value| value[:shortname] }

    TanukiEmoji.index.all.each do |emoji|
      expect(emoji.aliases & db_shortnames).to be_empty
    end
  end

  it 'verify emoji differences resolve correctly' do
    TanukiEmoji::Db::Gemojione::EMOJI_DIFFERENCES[:gemojione].each do |glyph|
      emoji = TanukiEmoji.find_by_codepoints(glyph)

      expect(emoji).to eq TanukiEmoji.find_by_alpha_code(emoji.alpha_code)
    end

    TanukiEmoji::Db::Gemojione::EMOJI_DIFFERENCES[:unicode].each do |glyph|
      emoji = TanukiEmoji.find_by_codepoints(glyph)

      expect(emoji).to eq TanukiEmoji.find_by_alpha_code(emoji.alpha_code)
    end
  end

  it 'makes gemojione codes into the primary alpha code' do
    emoji = TanukiEmoji.find_by_alpha_code(':thumbs_up:')

    expect(emoji.alpha_code).to eq(':thumbsup:')
    expect(emoji.aliases).to include(':thumbs_up:')
  end
end
