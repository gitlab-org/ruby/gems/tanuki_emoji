# frozen_string_literal: true

require 'rspec'

RSpec.describe TanukiEmoji::Db::AdditionalAliases do
  subject(:index) { described_class.new(index: TanukiEmoji.index) }

  before do
    TanukiEmoji.index.reset!(reload: false)

    TanukiEmoji::Db::EmojiTestParser.new(index: TanukiEmoji.index).load!
  end

  describe '#load!' do
    it 'populates indexed emojis with aliases' do
      emoji = TanukiEmoji.find_by_alpha_code('flag_england')

      expect(emoji.aliases).not_to include(':england:')
      expect(emoji.noto_image).to be_nil

      index.load!

      emoji = TanukiEmoji.find_by_alpha_code('flag_england')

      expect(emoji.aliases).to include(':england:')
      expect(emoji.noto_image).to eq 'gb-eng'
    end
  end
end
