# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe TanukiEmoji::Index do
  subject(:instance) { described_class.instance }

  let(:horse_emoji) do
    TanukiEmoji::Character.new('horse',
      codepoints: "\u{1f434}",
      alpha_code: ':horse:',
      description: 'horse face',
      category: 'Animals & Nature')
                          .tap do |emoji|
                            emoji.add_alias('horse_face')
                          end
  end

  describe '#add' do
    before do
      instance.reset!(reload: false)
    end

    context 'when emoji has not been indexed before' do
      it 'adds a new item to the index' do
        expect { instance.add(horse_emoji) }.to change { instance.all.size }.by(1)
      end
    end

    context 'with already indexed emoji with same codepoint' do
      it 'raises an error' do
        instance.add(horse_emoji)

        another_horse = TanukiEmoji::Character.new('another horse',
          codepoints: horse_emoji.codepoints,
          alpha_code: ':another_horse',
          description: 'another horse',
          category: 'Animals & Nature')

        expect { instance.add(another_horse) }.to raise_error(TanukiEmoji::CodepointAlreadyIndexedError)
      end
    end

    context 'with already indexed emoji with same alpha_code' do
      it 'raises an error' do
        instance.add(horse_emoji)

        similar_horse = TanukiEmoji::Character.new('similar horse',
          codepoints: horse_emoji.codepoints,
          alpha_code: 'horse',
          description: 'a very similar horse',
          category: 'Animals & Nature')

        expect { instance.add(similar_horse) }.to raise_error(TanukiEmoji::AlphaCodeAlreadyIndexedError)
      end
    end
  end

  describe '#update' do
    let(:modified_horse_emoji) do
      TanukiEmoji::Character.new('horse_face',
        codepoints: "\u{1f434}",
        alpha_code: ':horse_face:',
        description: 'horse face',
        category: 'Animals & Nature')
                            .tap do |emoji|
                              emoji.add_alias('horse')
                              emoji.add_alias('tall_horse')
                              emoji.add_codepoints("\u{1F984}")
                            end
    end

    before do
      instance.reset!(reload: false)
    end

    context 'when updating the index' do
      it 'adds a new item to the index' do
        instance.add(horse_emoji)

        expect { instance.update(modified_horse_emoji) }.not_to change { instance.all.size }

        emoji = instance.find_by_alpha_code('horse_face')

        expect(emoji.aliases).to include(':tall_horse:')
        expect(instance.find_by_alpha_code('tall_horse')).to eq emoji
        expect(instance.find_by_codepoints("\u{1F984}")).to eq emoji
      end
    end
  end

  describe '#find_by_alpha_code' do
    before do
      instance.reset!
    end

    it 'returns nil when nil value is provided' do
      expect(instance.find_by_alpha_code(nil)).to eq(nil)
    end

    it 'returns an emoji character when an indexed alpha code is provided' do
      expect(instance.find_by_alpha_code('horse_face')).to eq(horse_emoji)
    end
  end

  describe '#find_by_codepoints' do
    before do
      instance.reset!
    end

    it 'returns nil when nil value is provided' do
      expect(instance.find_by_codepoints(nil)).to eq(nil)
    end

    it 'returns an emoji character when an indexed alpha code is provided' do
      expect(instance.find_by_codepoints("\u{1f434}")).to eq(horse_emoji)
    end
  end

  describe '#alpha_code_pattern' do
    it 'matches an aliased alpha code' do
      expect(instance.alpha_code_pattern.match?(':+1:')).to be_truthy
    end

    it 'matches a regular indexed alpha code' do
      expect(instance.alpha_code_pattern.match?(':thumbsup:')).to be_truthy
    end
  end

  describe '#codepoints_pattern' do
    it 'matches an indexed emoji codepoints' do
      expect(instance.codepoints_pattern.match?('🐴')).to be_truthy
    end

    it 'matches an indexed emoji with requested text presentation' do
      expect(instance.codepoints_pattern.match?("🐴#{TanukiEmoji::Character::PLAIN_VARIATION_SELECTOR_STRING}")).to be_truthy
    end

    context 'with exclude_text_presentation = true' do
      it 'does not match an emoji with requested text presentation' do
        regex = instance.codepoints_pattern(exclude_text_presentation: true)

        expect(regex.match?("🐴#{TanukiEmoji::Character::PLAIN_VARIATION_SELECTOR_STRING}")).to be_falsy
      end
    end

    # `gsub`, as used by GitLab's Banzai::Filter::EmojiFilter#emoji_unicode_element_unicode_filter.
    it 'matches entire thumbsup_tone4 unicode' do
      expect('👍🏾'.gsub(instance.codepoints_pattern, '.')).to eq('.')
    end

    it 'matches entire family_mwgb unicode' do
      expect('👨‍👩‍👧‍👦'.gsub(instance.codepoints_pattern, '.')).to eq('.')
    end

    it 'matches entire woman_facepalming unicode' do
      expect('🤦‍♀️'.gsub(instance.codepoints_pattern, '.')).to eq('.')
    end
  end

  describe 'unicode versions' do
    before do
      instance.reset!
    end

    it 'populates indexed emojis with unicode data' do
      expect(TanukiEmoji.index.all.none? { |item| item.unicode_version.nil? }).to be_truthy
    end
  end
end
